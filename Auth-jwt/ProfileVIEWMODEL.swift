//
//  ProfileVIEWMODEL.swift
//  Auth-jwt
//
//  Created by eman alejilah on 28/02/1445 AH.
//

import Foundation


class ProfileVIEWMODEL : ObservableObject{
    
    @Published var email_user : String = "email"
    @Published var phones_user : String   = "phones_user"
    @Published var full_name_user  : String  = "full_name_user"
    
    init() {
        loadData()
    }
    
    func loadData() {
       DispatchQueue.main.async {
           
           let token1 = UserDefaults.standard.object(forKey: token_key) as? String
           let userSecret1 = UserDefaults.standard.object(forKey: secret_key) as? String

       guard let url = URL(string: "http://localhost:3000/apis/users") else { return }
       var request = URLRequest(url: url)
       request.httpMethod = "GET"
       request.addValue(userSecret1 ?? "", forHTTPHeaderField: "secret")
       request.addValue("application/json", forHTTPHeaderField: "Content-Type")
       request.addValue("Bearer \(token1 ?? "")", forHTTPHeaderField: "Authorization")

                   URLSession.shared.dataTask(with: request) {data, response, error in
                       
                       if let data = data {
                           print("data ::-> 1 ")
                           if let decodedResponse = try? JSONDecoder().decode(RootClass.self, from: data) {
                               print("data ::-> 2 " ,decodedResponse.data?.emails?[0].email ?? "" )
                               
                      
                               self.email_user = decodedResponse.data?.emails?[0].email ?? ""
                               self.full_name_user = decodedResponse.data?.fullName ?? ""
                               self.phones_user = decodedResponse.data?.phones?[0].number ?? ""
                               
                               
                               return
                           }
                       }
                       print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")

                       
                       
                       
                       
                       
                       
                       
                       ////////////
//                       guard let data = data  else {
//                           print(error?.localizedDescription ?? "No data")
//                           return
//                       }
//                       let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//                       if let responseJSON = responseJSON as? [String: Any] {
//                           let DD = responseJSON["data"] as? NSDictionary
//                           let emails = DD?.value(forKey: "emails") as? NSArray
//                           let phones = DD?.value(forKey: "phones") as? NSArray
//                           print("responseJSON = ",responseJSON)
//
//
//                           let email = (emails?[0] as AnyObject).value(forKey: "email") as? String
//
//                           self.email_user = email ?? ""
//
//
//                           let number = (phones?[0] as AnyObject).value(forKey: "number") as? String
//                           self.phones_user = number ?? ""
//
//                           let full_name = (DD?.value(forKey: "full_name")) as? String
//
//                           self.full_name_user = full_name ?? ""
//
//
//                           print("emails = ",self.email_user)
//
//                           print("phones = ",self.phones_user)
//
//                           print("full_name = ",self.full_name_user)
//                       }
                       
                       
                   }.resume()
       }
   }
    
}


