//
//  OTPView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 01/03/1445 AH.
//


    import SwiftUI
    
    struct OTPView: View {
        @ObservedObject var viewModel = OTPViewModel()
        @Environment(\.presentationMode) private var presentationMode
        
     
        @State var isShowingLoginView: Bool = false

        var body: some View {
            VStack {
                TextField("Enter code", text: $viewModel.code)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.bottom, 20)
                    
                
                Button("Verify OTP") {
                    viewModel.verifyOTP()
                    isShowingLoginView = true
                }
                .padding()
                .background(Color.blue)
                .foregroundColor(.white)
                .cornerRadius(8)
            }
            .fullScreenCover(isPresented: $isShowingLoginView) {
               LoginView()
           
           }
            .padding()
        }
    }


struct OTPView_Previews: PreviewProvider {
    static var previews: some View {
        OTPView()
    }
}
