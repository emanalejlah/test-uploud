//
//  GetMyProfileView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 26/02/1445 AH.
//

import SwiftUI

struct GetMyProfileView: View {
    @ObservedObject var viewModel = GetMyProfileViewModel()
    var body: some View {
        VStack{
            Button("Fetch Data") {
                viewModel.GetMyProfile()
            }
        }
    }
}

struct GetMyProfileView_Previews: PreviewProvider {
    static var previews: some View {
        GetMyProfileView()
    }
}
