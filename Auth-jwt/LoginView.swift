//
//  LoginView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 26/02/1445 AH.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject var viewModel : loginViewModel
    
    @Environment(\.presentationMode) private var presentationMode
    
 
    @State var isShowingGetProfileView: Bool = false
    
    init(viewModel1: loginViewModel = loginViewModel()) {
        self.viewModel = viewModel1
    }
    
    var body: some View {
        VStack {
  
             
            TextField("Identifier", text: $viewModel.identifier)
                 .padding()
                 .textFieldStyle(RoundedBorderTextFieldStyle())
                 
            
             SecureField("Password", text: $viewModel.password)
                 .padding()
                 .textFieldStyle(RoundedBorderTextFieldStyle())

            Button("Send Request") {
                DispatchQueue.main.async {
                    viewModel.sendPOSTRequest()
                   // isShowingGetProfileView = true
                }
               
               // isShowingGetProfileView = true
            }
            .padding()
        }
        .fullScreenCover(isPresented: $isShowingGetProfileView) {
           
                Dcode()
           
            
        }
        
        .padding()
        
        
        .onReceive(viewModel.$IsLogin) { isLogin in
            if isLogin{
                isShowingGetProfileView = true
            }
        }
    }
}
 

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
