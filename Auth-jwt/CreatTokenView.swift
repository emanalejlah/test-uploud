//
//  CreatTokenView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 26/02/1445 AH.
//
import SwiftUI

struct CreatTokenView: View {
    @ObservedObject var viewModel : TokenViewModel
    @Environment(\.presentationMode) private var presentationMode
    @State var isShowingRegisterView: Bool = false
    
    init(viewModel: TokenViewModel = TokenViewModel()) {
        self.viewModel = viewModel
        
    }

    var body: some View {
        VStack {
            Button("Send Request") {
                viewModel.sendPOSTRequest()
               // isShowingRegisterView = true
            }
            .padding()
        }
        .fullScreenCover(isPresented: $isShowingRegisterView) {
            RegisterView()
        }
        
        
        
        .onReceive(viewModel.$navigateToRegisterView) { ToRegisterView in
            if ToRegisterView{
                isShowingRegisterView = true
            }
        }
    }
}

struct CreatTokenView_Previews: PreviewProvider {
    static var previews: some View {
        CreatTokenView()
    }
}
