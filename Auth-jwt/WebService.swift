//
//  WebService.swift
//  Auth-jwt
//
//  Created by eman alejilah on 26/02/1445 AH.
//

import Foundation

import SwiftUI


let token_key = "token"
let secret_key = "secret"

let identifier_key = "identifier"
let user_key = "user"
//


class TokenViewModel: ObservableObject {
    @Published var navigateToRegisterView: Bool = false


    func sendPOSTRequest() {
        

        
        
        let key = "key"
        let applicationToken = "bc6880c1-59fc-40eb-817c-8344ed41b12b"

        UserDefaults.standard.setValue(applicationToken, forKey: key)
        let key1 = UserDefaults.standard.object(forKey: key) as? String
        
        
        let secreet = "secret"
        let applicationSecret = "b8242415-a94b-4c17-a07c-2f05f0bd8a71"
        
        UserDefaults.standard.setValue(applicationSecret, forKey: secreet)

        let deviceSecret1 = UserDefaults.standard.object(forKey: secreet) as? String
        
        
        

        
        
        
        let url = URL(string: "https://apimp.seapay.com.sa/apis/applications/create_token")!
        
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        request.setValue(key1 ?? "", forHTTPHeaderField: "key")
        
        request.setValue(deviceSecret1 ?? "", forHTTPHeaderField: "secret")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
                
                
                let DD = responseJSON["data"] as? NSDictionary
                if let jwt = DD?.value(forKey: "jwt") as? String{
                    UserDefaults.standard.set(jwt, forKey: token_key)
                }
                if let secret = DD?.value(forKey: "secret") as? String{
                    UserDefaults.standard.set(secret, forKey: secret_key)
                }
                UserDefaults.standard.synchronize()
                
                DispatchQueue.main.async {
                    self.navigateToRegisterView = true
                }
                
            }
        }
        
        task.resume()
//        DispatchQueue.main.async {
//               self.navigateToRegisterView = true
//           }
    }
    
}

//class loginDone : ObservableObject{
//    @Published var identifier: Bool
//}

class loginViewModel : ObservableObject{
  
    @Published var identifier: String = ""
    @Published var password: String = ""
    
    @Published var IsLogin : Bool = false
    
   
    
    func sendPOSTRequest() {
           
        
        DispatchQueue.main.async {
            
            
            
            //        let tokken = "token"
            //        let userToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQ2MTYwNjksImlhdCI6MTY5NDYwNDA2OX0.O2WsGS8uHZuOf3VJso3mmkuc6szNKBfSoiFix1iDA2g"
            //
            //        UserDefaults.standard.setValue(userToken, forKey: tokken)
            
            
            let token1 = UserDefaults.standard.object(forKey: token_key) as? String
            
            
            //        let secreet = "secret"
            //        let userSecret = "bd57e4ed-0bf2-4938-a25d-bff92cecbe31"
            //
            //        UserDefaults.standard.setValue(userSecret, forKey: secreet)
            
            let userSecret1 = UserDefaults.standard.object(forKey: secret_key) as? String
            
            
            
            
            
            
            //        let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQ0NzMxNjEsImlhdCI6MTY5NDQ2MTE2MX0.8cyD82aMwh9EbEKeZdYHCzdzvQC2prNkP8IFN-dimyE"
            //
            //        let secret = "34c5e5a0-f95b-46e8-8f21-e0e5862084ff"
            
            let url = URL(string: "http://localhost:3000/apis/users/login")!
            
            let json: [String: Any] = ["identifier": self.identifier, "password": self.password]
            
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            request.httpBody = jsonData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.setValue( "Bearer \(token1 ?? "" )", forHTTPHeaderField: "Authorization")
            
            request.setValue(userSecret1 ?? "" , forHTTPHeaderField: "secret")
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error?.localizedDescription ?? "No data")
                    
                    return
                }
                
                let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
                if let responseJSON = responseJSON as? [String: Any] {
                    print(responseJSON)
                    if let responseJSON = responseJSON as? [String: Any] {
                        
//                        print(responseJSON)
                        
                        
                        
                        if let http_code = responseJSON["http_code"] as? Int{
                            
                            if http_code != 200{
                                //user_message
                                if let user_message = responseJSON["user_message"] as? NSArray{
                                    //message
                                    if let messages = user_message[0] as? NSDictionary{
                                        let message = messages.value(forKey: "message") as? String
                                        print("message == \(message ?? "")")
                                    }
                                }
                                
                                return
                            }
                        }
                        
                        
                        if let DD = responseJSON["data"] as? [String: Any] {
                            
                            print("data == \(DD)")
                            
                            if let jwt = DD["jwt"] as? String {
                                print("jwt == \(jwt)")
                                UserDefaults.standard.set(jwt, forKey: token_key)
                            }
                            
                            if let secret = DD["secret"] as? String {
                                print("secret == \(secret)")
                                UserDefaults.standard.set(secret, forKey: secret_key)
                            }
                            
                            UserDefaults.standard.synchronize()
                            
                            
                            self.IsLogin = true
                            
                            // self.IsLogin?(true)
                            
                        }
                        
                        
                        print("self.IsLogin = true ")
                        self.IsLogin = true
                        //                    if let jwt = DD?.value(forKey: "jwt") as? String{
                        //                        UserDefaults.standard.set(jwt, forKey: token_key)
                        //                    }
                        //                    if let secret = DD?.value(forKey: "secret") as? String{
                        //                        UserDefaults.standard.set(secret, forKey: secret_key)
                        //                    }
                        //                    UserDefaults.standard.synchronize()
                        
                    }
                }
            }
            task.resume()
            
        }
    }
}
    
class GetMyProfileViewModel: ObservableObject {
    
    func GetMyProfile() {
        
        
     
        
        let token1 = UserDefaults.standard.object(forKey: token_key) as? String
        let userSecret1 = UserDefaults.standard.object(forKey: secret_key) as? String
        
    
        let url = URL(string: "http://localhost:3000/apis/users")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(userSecret1 ?? "", forHTTPHeaderField: "secret")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(token1 ?? "")", forHTTPHeaderField: "Authorization")
        
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data  else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }
        
        task.resume()
    }
}



class RegisterViewModel: ObservableObject {
    @Published var fullName: String = ""
    @Published var email: String = ""
        @Published var countryCode: String = ""
    @Published var phoneNumber: String = ""

    @Published var password: String = ""
    
//        @Published var fullName: String = ""
//        @Published var email: String = ""
//
//        @Published var countryCode: String = ""
//    @Published var phoneNumber: String = ""
//      @Published var password: String = ""
    
    func sendPOSTRequestForRegistration() {
        let url = URL(string: "https://apimp.seapay.com.sa/apis/users/register")!
        
        let json: [String: Any] = [
            "full_name": fullName,
            "emails": [["email": email]],
            "phones": [["country_code": countryCode, "number": phoneNumber]],
            "credentials": [["password": password]]
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else { return }
       
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        
        let token = UserDefaults.standard.object(forKey: token_key) as? String
        let secret = UserDefaults.standard.object(forKey: secret_key) as? String
        
        
        
        
        request.setValue("Bearer \(token ?? "")", forHTTPHeaderField: "Authorization")
        request.setValue(secret ?? "", forHTTPHeaderField: "secret")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
        
            if let data = data {
                print("data ::-> 1")
                if let decodedResponse = try? JSONDecoder().decode(registerModel.self, from: data) {
                  print(decodedResponse)
                    
                    
                    
                    
                    if let id = decodedResponse.data?.id as? String{
                        UserDefaults.standard.set(id, forKey: user_key)
                    }
                    
                    if let phone_id = decodedResponse.data?.phones?[0].id as? String{
                        UserDefaults.standard.set(phone_id, forKey: identifier_key)
                    }
                    UserDefaults.standard.synchronize()
                    
                    //user
                    print("data id ::-> 2 " ,decodedResponse.data?.id ?? "" )
                    
                    print("phones id = ",decodedResponse.data?.phones?[0].id ?? "")
                    // self.users = decodedResponse.users
//                    self.email_user = decodedResponse.data?.emails?[0].email ?? ""
//                    self.full_name_user = decodedResponse.data?.fullName ?? ""
//                    self.phones_user = decodedResponse.data?.phones?[0].number ?? ""
                    
                    
                    return
                }
            }
            
            
            
            
            
            
            
//            guard let data = data, error == nil else {
//                print(error?.localizedDescription ?? "No data")
//                return
//            }
            
            
        
            
        //    let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//            if let responseJSON = responseJSON as? [String: Any] {
//                print(responseJSON)
//                if let responseJSON = responseJSON as? [String: Any] {
//                    let DD = responseJSON["data"] as? NSDictionary
//                    if let jwt = DD?.value(forKey: "jwt") as? String{
//                        UserDefaults.standard.set(jwt, forKey: token_key)
//                    }
//                    if let secret = DD?.value(forKey: "secret") as? String{
//                        UserDefaults.standard.set(secret, forKey: secret_key)
//                    }
//                    UserDefaults.standard.synchronize()
//
//                }
//            }
        }
        
        task.resume()
    }
}


class OTPViewModel: ObservableObject {
    @Published var code: String = ""
//    @Published var identifier: String = "6505e84390cedd85e39e79cf"
    @Published var whatFor: String = "mobile_verification"
//@Published var user: String = "6505e84390cedd85e39e79cd"
    
    let identifier = UserDefaults.standard.object(forKey: identifier_key) as? String
    let user = UserDefaults.standard.object(forKey: user_key) as? String
    
    func verifyOTP() {
        print("11111")
        guard let url = URL(string: "http://localhost:3000/apis/otps/verify") else { return }
        print("222")
        let json: [String: Any] = ["code": code, "identifier": identifier ?? "", "what_for": whatFor, "user": user ?? ""]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else { return }
        print("33")
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let token = UserDefaults.standard.string(forKey: token_key) ?? ""
        let secret = UserDefaults.standard.string(forKey: secret_key) ?? ""
        
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.addValue(secret, forHTTPHeaderField: "secret")
        
        print("code == \(code)")
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            print("4444")
            print("data otp == \(data)")
            guard let data = data else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            
            print("555")
            
            guard let responseJSON = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                print("Error parsing JSON")
                return
            }
            print("6666")
            print("responseJSON = \(responseJSON)")
            
            
            if let DD = responseJSON["data"] as? [String: Any] {
                
                print("data == \(DD)")
                
                if let jwt = DD["jwt"] as? String {
                    UserDefaults.standard.set(jwt, forKey: token_key)
                }
                
                if let secret = DD["secret"] as? String {
                    UserDefaults.standard.set(secret, forKey: secret_key)
                }
                
                UserDefaults.standard.synchronize()
                
                if let id = DD["_id"] as? String {
                    UserDefaults.standard.set(id, forKey: "identifier")
                    print("ID is: \(id)")
                }
                
                if let phones = DD["phones"] as? [[String: Any]], let firstPhone = phones.first {
                    if let phoneId = firstPhone["_id"] as? String {
                        UserDefaults.standard.set(phoneId, forKey: "phone_id")
                        print("Phone ID is: \(phoneId)")
                    }
                }
            }
        }
        
        task.resume()
    }
}


//
//
//["stack_trace": <null>, "user_message": <__NSArrayI 0x600001a9ba00>(
//{
//    language = en;
//    message = "The request was processed correctly";
//},
//{
//    language = en;
//    message = "\U062a\U0645 \U0645\U0639\U0627\U0644\U062c\U0629 \U0627\U0644\U0637\U0644\U0628 \U0628\U0634\U0643\U0644 \U0627\U0644\U0635\U062d\U064a\U062d";
//}
//)
//, "code": 1200, "property": <null>, "is_technical_error": 0, "function": <null>, "developer_message": The request was processed correctly, "http_code": 200, "users": <null>, "data": {
//    "__v" = 0;
//    "_id" = 6505e84390cedd85e39e79cd;
//    "access_tokens" =     (
//    );
//    bio = "SeaPay User";
//    "create_details" =     {
//        when = "2023-09-16T17:39:15.080Z";
//        who = 64ef31ac585f4bbcf9e570ea;
//    };
//    credentials =     (
//                {
//            "_id" = 6505e84390cedd85e39e79d0;
//            "created_details" =             {
//                when = "2023-09-16T17:39:15.138Z";
//                who = "<null>";
//            };
//            "expired_date" = "<null>";
//            "is_active" = 1;
//            "is_expired" = 0;
//            "is_temporary" = 0;
//            password = "$2b$10$orUi.aA00yMqx.8hK6hsRu/WObL.EF72K2mokqPeAHvn13RfFpcNi";
//            salt = "$2b$10$orUi.aA00yMqx.8hK6hsRu";
//        }
//    );
//    "delete_details" =     {
//        when = "2023-09-16T17:39:15.138Z";
//    };
//    emails =     (
//                {
//            "_id" = 6505e84390cedd85e39e79ce;
//            "create_date" = "2023-09-16T17:39:15.138Z";
//            "deleted_date" = "<null>";
//            email = "mohammed232@gmail.com";
//            "is_active" = 1;
//            "is_deleted" = 0;
//            "is_verified" = 0;
//            "verified_date" = "<null>";
//        }
//    );
//    "full_name" = "mohammed ahmed ahmed";
//    image = "default.png";
//    "is_active" = 0;
//    "is_deleted" = 0;
//    "is_listed" = 1;
//    "is_otp_enabled" = 0;
//    "is_rest_password_required" = 0;
//    "otp_settings" = "<null>";
//    phones =     (
//                {
//            "_id" = 6505e84390cedd85e39e79cf;
//            "country_code" = 966;
//            "create_date" = "2023-09-16T17:39:15.138Z";
//            "deleted_date" = "<null>";
//            "is_active" = 1;
//            "is_deleted" = 0;
//            "is_verified" = 0;
//            number = 596326652;
//            "verified_date" = "<null>";
//        }
//    );
//    "preferred_language" = en;
//    "requested_to_rest_password" =     (
//    );
//    role = owners;
//    tokens =     (
//    );
//    "update_details" =     (
//    );
//}, "is_error": 0]



//
//            do {
//                let responseJSON = try JSONSerialization.jsonObject(with: data, options: [])
//
//                if let responseDict = responseJSON as? [String: Any],
//                   let dataDict = responseDict["data"] as? [String: Any],
//                   let jwt = dataDict["jwt"] as? String,
//                   let secret = dataDict["secret"] as? String {
//
//                    // Save values to UserDefaults
//                    UserDefaults.standard.set(jwt, forKey: "jwt_key")
//                    UserDefaults.standard.set(secret, forKey: "secret_key")
//                    UserDefaults.standard.synchronize()
//
//                    // Print values
//                    print("JWT: \(jwt)")
//                    print("Secret: \(secret)")
//
//                    DispatchQueue.main.async {
//                        completion(.success(responseDict))
//                    }
//                } else {
//                    completion(.failure(.parsingError(NSError(domain: "Invalid JSON format", code: 0, userInfo: nil))))
//                }
//            } catch let parsingError {
//                completion(.failure(.parsingError(parsingError)))
//            }
