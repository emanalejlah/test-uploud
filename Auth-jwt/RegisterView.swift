//
//  RegisterView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 01/03/1445 AH.
//

import SwiftUI

struct RegisterView: View {
    @ObservedObject var viewModel = RegisterViewModel()
    @Environment(\.presentationMode) private var presentationMode
    
 
    @State var isShowingOtpView: Bool = false
    
    var body: some View {
        
        VStack{
            
            CustomTextField1(systemName: "person.circle.fill", placeholder: "Full Name", text: $viewModel.fullName)
            CustomTextField1(systemName: "phone.circle.fill", placeholder: "email", text: $viewModel.email)
            CustomTextField1(systemName: "phone.circle.fill", placeholder: "countryCode", text: $viewModel.countryCode)
            CustomTextField1(systemName: "phone.circle.fill", placeholder: "Phone", text: $viewModel.phoneNumber)
            CustomTextField1(systemName: "phone.circle.fill", placeholder: "passwoord", text: $viewModel.password)
        
            
            
            
            
            Button("Register") {
                viewModel.sendPOSTRequestForRegistration()
                isShowingOtpView = true
                
            }
            
        }    .fullScreenCover(isPresented: $isShowingOtpView) {
            OTPView()
        
        }
    }
}

struct CustomTextField1: View {
    var systemName: String
    var placeholder: String
    @Binding var text: String
    
    var body: some View {
        HStack {
            TextField(placeholder, text: $text)
            
            Image(systemName: systemName)
                .foregroundColor(text.isEmpty ? (Color("P2")) : (Color("P1")))
              
                .font(Font.system(size: 28))
            
        }
        
        .padding(.vertical, 8)
        .padding(.horizontal)
        .frame(width: 355, height: 62)
        .background(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}

struct CustomSecureField2: View {
    var systemName: String
    var placeholder: String
    @Binding var text: String
    
    var body: some View {
        HStack {
            SecureField(placeholder, text: $text)
            
            Image(systemName: systemName)
                .foregroundColor(text.isEmpty ? (Color("P2")) : (Color("P1")))
                .font(Font.system(size: 28))
              
        }
        
        .padding(.vertical, 8)
        .padding(.horizontal)
        .frame(width: 355, height: 62)
        .background(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}

extension String {
    var isNumeric: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
