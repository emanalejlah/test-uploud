//
//  NewOtpView.swift
//  Auth-jwt
//
//  Created by eman alejilah on 01/03/1445 AH.
//

//import SwiftUI
//
//struct NewOtpView: View {
//    @ObservedObject var viewModel = NewOtp()
//
//    var body: some View {
//        VStack {
//            Text("OTP Verification")
//                .font(.largeTitle)
//                .padding(.bottom, 20)
//
//            VStack(alignment: .leading) {
//                Text("Receiver ID:")
//                TextField("Enter Receiver ID", text: $viewModel.receiverID)
//                    .textFieldStyle(RoundedBorderTextFieldStyle())
//                    .padding(.bottom, 10)
//                
//                Text("Channel:")
//                TextField("Enter Channel (e.g., mobile)", text: $viewModel.channel)
//                    .textFieldStyle(RoundedBorderTextFieldStyle())
//                    .padding(.bottom, 10)
//
//                Text("What For:")
//                TextField("Enter Purpose (e.g., mobile_verification)", text: $viewModel.whatFor)
//                    .textFieldStyle(RoundedBorderTextFieldStyle())
//                    .padding(.bottom, 10)
//            }
//            .padding()
//
//            Button("Verify OTP") {
//                viewModel.verifyOTP()
//            }
//            .padding(.all, 12)
//            .background(Color.blue)
//            .foregroundColor(.white)
//            .cornerRadius(8)
//        }
//    }
//}
//
//struct NewOtpView_Previews: PreviewProvider {
//    static var previews: some View {
//        NewOtpView()
//    }
//}
