//
//  registerModel.swift
//  Auth-jwt
//
//  Created by eman alejilah on 02/03/1445 AH.
//

import Foundation

struct registerModel : Codable {

    let code : String?
    let data : dataUser?
    let developerMessage : String?
    let function : String?
    let httpCode : Int?
    let isError : Bool?
    let isTechnicalError : Bool?
    let property : String?
    let stackTrace : String?
    let userMessage : [UserMessage]?
    let users : String?


    enum CodingKeys: String, CodingKey {
        case code = "code"
        case data
        case developerMessage = "developer_message"
        case function = "function"
        case httpCode = "http_code"
        case isError = "is_error"
        case isTechnicalError = "is_technical_error"
        case property = "property"
        case stackTrace = "stack_trace"
        case userMessage = "user_message"
        case users = "users"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        data = try values.decodeIfPresent(dataUser.self, forKey: .data)
        //dataUser
        //data = try Data(from: decoder)
        developerMessage = try values.decodeIfPresent(String.self, forKey: .developerMessage)
        function = try values.decodeIfPresent(String.self, forKey: .function)
        httpCode = try values.decodeIfPresent(Int.self, forKey: .httpCode)
        isError = try values.decodeIfPresent(Bool.self, forKey: .isError)
        isTechnicalError = try values.decodeIfPresent(Bool.self, forKey: .isTechnicalError)
        property = try values.decodeIfPresent(String.self, forKey: .property)
        stackTrace = try values.decodeIfPresent(String.self, forKey: .stackTrace)
        userMessage = try values.decodeIfPresent([UserMessage].self, forKey: .userMessage)
        users = try values.decodeIfPresent(String.self, forKey: .users)
    }


}

//struct UserMessage : Codable {
//
//    let language : String?
//    let message : String?
//
//
//    enum CodingKeys: String, CodingKey {
//        case language = "language"
//        case message = "message"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        language = try values.decodeIfPresent(String.self, forKey: .language)
//        message = try values.decodeIfPresent(String.self, forKey: .message)
//    }
//
//
//}

//struct Data : Codable {
//
//    let v : Int?
//    let id : String?
//    let accessTokens : [String]?
//    let bio : String?
//    let createDetails : CreateDetail?
//    let credentials : [Credential]?
//    let deleteDetails : DeleteDetail?
//    let emails : [Email]?
//    let fullName : String?
//    let image : String?
//    let isActive : Bool?
//    let isDeleted : Bool?
//    let isListed : Bool?
//    let isOtpEnabled : Bool?
//    let isRestPasswordRequired : Bool?
//    let otpSettings : String?
//    let phones : [Phone]?
//    let preferredLanguage : String?
//    let requestedToRestPassword : [String]?
//    let role : String?
//    let tokens : [String]?
//    let updateDetails : [String]?
//
//
//    enum CodingKeys: String, CodingKey {
//        case v = "__v"
//        case id = "_id"
//        case accessTokens = "access_tokens"
//        case bio = "bio"
//        case createDetails
//        case credentials = "credentials"
//        case deleteDetails
//        case emails = "emails"
//        case fullName = "full_name"
//        case image = "image"
//        case isActive = "is_active"
//        case isDeleted = "is_deleted"
//        case isListed = "is_listed"
//        case isOtpEnabled = "is_otp_enabled"
//        case isRestPasswordRequired = "is_rest_password_required"
//        case otpSettings = "otp_settings"
//        case phones = "phones"
//        case preferredLanguage = "preferred_language"
//        case requestedToRestPassword = "requested_to_rest_password"
//        case role = "role"
//        case tokens = "tokens"
//        case updateDetails = "update_details"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        v = try values.decodeIfPresent(Int.self, forKey: .v)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        accessTokens = try values.decodeIfPresent([String].self, forKey: .accessTokens)
//        bio = try values.decodeIfPresent(String.self, forKey: .bio)
//        createDetails = try CreateDetail(from: decoder)
//        credentials = try values.decodeIfPresent([Credential].self, forKey: .credentials)
//        deleteDetails = try DeleteDetail(from: decoder)
//        emails = try values.decodeIfPresent([Email].self, forKey: .emails)
//        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
//        image = try values.decodeIfPresent(String.self, forKey: .image)
//        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
//        isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
//        isListed = try values.decodeIfPresent(Bool.self, forKey: .isListed)
//        isOtpEnabled = try values.decodeIfPresent(Bool.self, forKey: .isOtpEnabled)
//        isRestPasswordRequired = try values.decodeIfPresent(Bool.self, forKey: .isRestPasswordRequired)
//        otpSettings = try values.decodeIfPresent(String.self, forKey: .otpSettings)
//        phones = try values.decodeIfPresent([Phone].self, forKey: .phones)
//        preferredLanguage = try values.decodeIfPresent(String.self, forKey: .preferredLanguage)
//        requestedToRestPassword = try values.decodeIfPresent([String].self, forKey: .requestedToRestPassword)
//        role = try values.decodeIfPresent(String.self, forKey: .role)
//        tokens = try values.decodeIfPresent([String].self, forKey: .tokens)
//        updateDetails = try values.decodeIfPresent([String].self, forKey: .updateDetails)
//    }
//
//
//}

